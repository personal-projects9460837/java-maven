def ansibleSERVER = 'ubuntu@10.0.4.122'
pipeline {
    agent any 
    tools {
        maven 'maven'
    } 
    
    stages {
        stage('Code Analysis') {
            steps {
                withSonarQubeEnv('sonar') {
                sh "mvn sonar:sonar -Dsonar.java.binaries=target/classes"
                }
            }   
        }

        stage('code quality approval'){
            steps{
                timeout(time: 2, unit: 'MINUTES') {
                  waitForQualityGate abortPipeline: true
               }
            }
        } 

        stage ("Incrementing version") {
            steps {
                script {
                    echo "Incrementing version"
                    sh "mvn build-helper:parse-version versions:set \
                    -DnewVersion=\\\${parsedVersion.majorVersion}.\\\${parsedVersion.minorVersion}.\\\${parsedVersion.nextIncrementalVersion} \
                    versions:commit"
                    def matcher = readFile('pom.xml') =~ '<version>(.+)</version>'
                    def version = matcher[0][1]
                    env.IMAGE = "$version-$BUILD_NUMBER"
                }
            }
        }

        stage ("commit new version to github") {
            steps {
                script {
                    echo "committing new version to github"
                    withCredentials([usernamePassword(credentialsId:'github-id', usernameVariable: 'USER', passwordVariable: 'PASS')]) {
                        sh 'git config --global user.email "jenkins@example.com"' 
                        sh 'git config --global user.name "jenkins"'

                        sh "git status"
                        sh "git branch"

                        sh "git remote set-url origin https://$USER:$PASS@github.com/davacho/java-maven.git"
                        sh "git add ."
                        sh 'git commit -m "jenkins commit"'
                        sh "git push origin HEAD:main"
                    }

                }
            }
        }

        stage('Build Code'){
            steps{
                echo "packaging the application"
                sh 'mvn clean package -Dmaven.test.skip'
            }
        }
        
        stage('Copying value of environment variable into /tmp/.auth'){
            steps{
                script {
                    sh "echo ${IMAGE} > /tmp/.auth"
                }
            } 
        } 

        stage('Send Artifacts to Ansible Server') {
            steps { 
                sshagent(['ansible-key']) {
                    sh "scp -o StrictHostKeyChecking=no target/spring-petclinic-*.war  ${ansibleSERVER}:/home/ubuntu"
                    sh "scp -o StrictHostKeyChecking=no /tmp/.auth ${ansibleSERVER}:/tmp/.auth"
                    sh "ssh -o StrictHostKeyChecking=no ${ansibleSERVER} /home/ubuntu/proj.sh"
                }
            }
        }

        stage('Trigger Stage Playbooks') {
            steps { 
                script {
                    def ansibleSTAGE = 'ansible-playbook playbooks/stage.yaml'
                    sshagent(['ansible-key']) {
                        sh "ssh -o StrictHostKeyChecking=no ${ansibleSERVER} ${ansibleSTAGE}"
                    }
                }
            }
        }

        stage ('Deploy To Prod'){
            input{
                message "Do you want to proceed with production deployment?"
           }
            steps { 
                script {
                    def ansiblePROD   = 'ansible-playbook playbooks/prod.yaml'
                    sshagent(['ansible-key']) {
                        sh "ssh -o StrictHostKeyChecking=no ${ansibleSERVER} ${ansiblePROD}"
                    }
                }
            }
        }

        stage('Sending email'){
            steps{
                mail (body: 'Deployment was successful', subject: 'Jenkins message', to: 'davcho2014@gmail.com')
            } 
        } 

    }
}

